import logo from "./logo.svg";
import "./App.css";
import Ex_header from "./header/Ex_header";
import Ex_banner from "./banner/Ex_banner";
import Ex_card from "./cards/Ex_card";
import Ex_footer from "./footer/Ex_footer";

function App() {
  return (
    <div>
      <div className="container">
        <Ex_header />
        <div className="Carousel m-5 ">
          <Ex_banner />
        </div>
        <div style={{ display: "flex", gap: "20px" }} className="cards ">
          <Ex_card></Ex_card>
          <Ex_card></Ex_card>
          <Ex_card></Ex_card>
          <Ex_card></Ex_card>
        </div>
      </div>

      <footer
        style={{
          textAlign: "center",
          backgroundColor: "gray",
          height: "50px",
          display: "grid",
          alignItems: "center",
        }}
        className="mt-5"
      >
        <Ex_footer />
      </footer>
    </div>
  );
}

export default App;
