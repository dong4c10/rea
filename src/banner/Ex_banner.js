import React, { Component } from "react";

export default class Ex_banner extends Component {
  render() {
    return (
      <div>
        <div className="card flex">
          <div className="card-header">Featured</div>
          <div className="card-body">
            <h1>A Warm Welcome</h1>
            <h5 className="card-title">Special title treatment</h5>
            <p className="card-text">
              With supporting text below as a natural lead-in to additional
              content.
            </p>
            <a href="#" className="btn btn-primary">
              Go somewhere
            </a>
          </div>
        </div>
      </div>
    );
  }
}
